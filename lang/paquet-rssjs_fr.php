<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// R
	'rssjs_description' => 'Affichage dans l\'espace public de flux Rss grâce à l\'API Google Feed, à partir de sites syndiqués ou de l\'url du flux',
	'rssjs_slogan' => 'Affichage de flux Rss grâce à l\'API Google Feed',
);
?>